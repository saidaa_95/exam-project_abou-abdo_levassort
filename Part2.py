import pandas as pd
from sklearn.cluster import KMeans
import matplotlib.pyplot as plt

# Import the dataset from previous part
from Part1 import df

# Create a new dataset use useful data
new_df = pd.DataFrame(data=df, columns= ["FIRST_COURSE","SECOND_COURSE","THIRD_COURSE"])

X = new_df
# clustering with Kmeans
kmeans = KMeans(n_clusters=4).fit(new_df)
labels = kmeans.labels_

# adding the new column

new_df["Label_kmeans"] = labels

# plotting of the graph

fig = plt.figure(figsize=(15,10))
ax = fig.add_subplot(111, projection="3d")

ax.scatter(new_df.FIRST_COURSE[new_df.Label_kmeans == 0], new_df["SECOND_COURSE"][new_df.Label_kmeans == 0],
           new_df["THIRD_COURSE"][new_df.Label_kmeans == 0], c="blue", s=100,linestyle="-")
ax.scatter(new_df.FIRST_COURSE[new_df.Label_kmeans == 1], new_df["SECOND_COURSE"][new_df.Label_kmeans == 1],
           new_df["THIRD_COURSE"][new_df.Label_kmeans == 1], c="red",edgecolor="k", s=100, linestyle="-")
ax.scatter(new_df.FIRST_COURSE[new_df.Label_kmeans == 2], new_df["SECOND_COURSE"][new_df.Label_kmeans == 2],
           new_df["THIRD_COURSE"][new_df.Label_kmeans == 2], c="green",edgecolor="k", s=100, linestyle="-")
ax.scatter(new_df.FIRST_COURSE[new_df.Label_kmeans == 3], new_df["SECOND_COURSE"][new_df.Label_kmeans == 3],
           new_df["THIRD_COURSE"][new_df.Label_kmeans == 3], c="orange", edgecolor="k", s=100, linestyle="-")
centers = kmeans.cluster_centers_
ax.scatter(centers[:, 0], centers[:, 1], centers[:, 2], c="black", s=500);
plt.xlabel("FIRST_COURSE")
plt.ylabel("SECOND_COURSE")
ax.set_zlabel("THIRD_COURSE")
ax.legend(['Cluster 0', 'Cluster 1', 'Cluster 2', 'Cluster 3'], fontsize=12)
ax.set_title('Labelled Data')
plt.show()

# With this graph we can identity which cluster relates to health, retirement, business and normal group