import time
from datetime import datetime
import numpy as np
from os.path import exists
import pandas as pd
import random

# main function to initiate the restaurant simulation and enter clients manually
def main():
    # make a random id for each customer
    clientid = "ID" + str(random.randrange(100000, 999999))
    client = RestClient(clientid, "business")
    client.goToRestaurant()


# class restaurant courses to save and update client data from the restaurant simulation
class RestCourses:
    first_order = []
    second_order = []
    third_order = []
    c_id = []
    order_time = []
    df_new = pd.DataFrame()

    def __init__(self, order):
        self.first_order.append(order[0])
        self.second_order.append(order[1])
        self.third_order.append(order[2])
        self.order_time.append(order[3])
        self.c_id.append(order[4])
        self.updateDataFrame()

    def updateDataFrame(self):
        df_id = np.array(self.c_id, dtype='object')
        self.df_new["id"] = df_id.tolist()

        df_course1 = np.array(self.first_order, dtype='object')
        self.df_new["first course"] = df_course1.tolist()

        df_course2 = np.array(self.second_order, dtype='object')
        self.df_new["second course"] = df_course2.tolist()

        df_course3 = np.array(self.third_order, dtype='object')
        self.df_new["third course"] = df_course3.tolist()

        df_time = np.array(self.order_time, dtype='object')
        self.df_new["time"] = df_time.tolist()


# class restaurant client to simulate a restaurant like experience
class RestClient:
    type = ""
    idList = []
    id = ""
    first_course = ["Soup", "Tomato-Mozarella", "Oysters"]
    second_course = ["Salad", "Spaghetti", "Steak", "Lobster"]
    third_course = ["Ice cream", "Pie"]
    course1 = ""
    course2 = ""
    course3 = ""

    # constructor that checks and creates a new client
    def __init__(self, c_id, c_type):
        new_client = True
        for client in self.idList:
            if c_id == client:
                new_client = False
        if new_client:
            self.idList.append(c_id)
            self.id = c_id
            self.type = c_type

    # function to simulate the restaurant simulation
    def goToRestaurant(self):
        print("Welcome to the restaurant simulation\n"
              "Menu: \n\t"
              "Starter:\n\t\t*" + self.first_course[0] + ":1\n\t\t*" + self.first_course[1] + ":2\n\t\t*" +
              self.first_course[2] +
              ":3\n\t"
              "Main:\n\t\t*" + self.second_course[0] + ":1\n\t\t*" + self.second_course[1] + ":2\n\t\t*" +
              self.second_course[2] +
              ":3\n\t\t*" + self.second_course[3] + ":4\n\t"
                                                    "Dessert:\n\t\t*" + self.third_course[0] + ":1\n\t\t*" +
              self.third_course[1] + ":2")

        # starter course input
        print("What would you like for Starter?(Choose by its number, if you do not want a certain course enter 0)")
        # check input validity and add starter order
        is_int = False
        while not is_int:
            try:
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.first_course[0] + ":1\n\t\t*" + self.first_course[1]
                      + ":2\n\t\t*" + self.first_course[2] + ":3")
                order1 = int(input("enter number here:"))
                is_int = True
                if order1 == 0:
                    self.course1 = "none"
                    print("You have chosen " + self.course1)
                elif order1 == 1:
                    self.course1 = self.first_course[0]
                    print("You have chosen " + self.course1)
                elif order1 == 2:
                    self.course1 = self.first_course[1]
                    print("You have chosen " + self.course1)
                elif order1 == 3:
                    self.course1 = self.first_course[2]
                    print("You have chosen " + self.course1)
                else:
                    print("invalid input")
                    is_int = False
            except ValueError:
                print("invalid input")
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.first_course[0] + ":1\n\t\t*" + self.first_course[1]
                      + ":2\n\t\t*" + self.first_course[2] + ":3")
                is_int = False

        # main course input
        print("What would you like for Main?(Choose by its number, if you do not want a certain course enter 0)")
        # check input validity and add main order
        is_int = False
        while not is_int:
            try:
                order2 = int(input("enter number here:"))
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.second_course[0] + ":1\n\t\t*" + self.second_course[1] +
                      ":2\n\t\t*" + self.second_course[2] + ":3\n\t\t*" + self.second_course[3] + ":4")
                is_int = True
                if order2 == 0:
                    self.course2 = "none"
                    print("You have chosen " + self.course2)
                elif order2 == 1:
                    self.course2 = self.second_course[0]
                    print("You have chosen " + self.course2)
                elif order2 == 2:
                    self.course2 = self.second_course[1]
                    print("You have chosen " + self.course2)
                elif order2 == 3:
                    self.course2 = self.second_course[2]
                    print("You have chosen " + self.course2)
                elif order2 == 4:
                    self.course2 = self.second_course[3]
                    print("You have chosen " + self.course2)
                else:
                    print("invalid input")
                    is_int = False
            except ValueError:
                print("invalid input")
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.second_course[0] + ":1\n\t\t*" + self.second_course[1] +
                      ":2\n\t\t*" + self.second_course[2] + ":3\n\t\t*" + self.second_course[3] + ":4")
                is_int = False

        # dessert course input
        print("What would you like for Dessert?(Choose by its number, if you do not want a certain course enter 0)")
        # check input validity and add dessert order
        is_int = False
        while not is_int:
            try:
                print("Options:\n\t\t*Nothing:0\n\t\t*" + self.third_course[0] + ":1\n\t\t*" + self.third_course[1] +
                      ":2")
                order3 = int(input("enter number here:"))
                is_int = True
                if order3 == 0:
                    self.course3 = "none"
                    print("You have chosen " + self.course3)
                elif order3 == 1:
                    self.course3 = self.third_course[0]
                    print("You have chosen " + self.course3)
                elif order3 == 2:
                    self.course3 = self.third_course[1]
                    print("You have chosen " + self.course3)
                else:
                    print("invalid input")
                    is_int = False
            except ValueError:
                print("invalid input")
                is_int = False

        # get current time
        timestamp = time.time()
        # convert to datetime
        date_time = datetime.fromtimestamp(timestamp)
        # convert timestamp to string in dd-mm-yyyy HH:MM:SS
        str_date_time = date_time.strftime("%d-%m-%Y, %H:%M:%S")

        # save the order and get it as an object
        df_final = RestCourses([self.course1, self.course2, self.course3, str_date_time, self.id])

        # create or append the orders to a csv file
        if exists("Data/part4.csv"):
            df_old = pd.read_csv("Data/part4.csv", index_col=0)
            df_old.loc[len(df_old.index)] = [self.id, self.course1, self.course2, self.course3, str_date_time]
            df_old.to_csv('Data/part4.csv', index=True)
        else:
            df_final.df_new.to_csv('Data/part4.csv')


if __name__ == '__main__':
    main()

# This part is used for the mass simulation
Id_list = []
Type_list = []
starter_list = []
main_list = []
dessert_list = []

# Creation of the function


def gorestaurant(ID, Type, Starter, main, dessert):
    l = [ID, Type, Starter, main, dessert]
    return l


# Random variable that will store the client type
Type = "Nothing"
Starter = "Nothing"
main = "Nothing"
dessert = "Nothing"

# create a data set of 20 courses per day, 365 days per year, 5 years
n = 20 * 365 * 5

for i in range(1, n):
    Rnd = random.randrange(1, 100)

    # We have 20% of healthy, business and retirement, 40% of Onetime clients
    if Rnd <= 20:
        Type = "Business"
        # Business return 50% of the times, yes means they already came
        if Rnd <= 10:
            clientid = "returning"
        else:
            clientid = "ID" + str(random.randrange(100000, 999999))


    elif Rnd <= 40:
        Type = "Healthy"
        # Healthy return 70% of the times, yes means they already came
        if Rnd <= 35:
            clientid = "returning"
        else:
            clientid = "ID" + str(random.randrange(100000, 999999))

    elif Rnd <= 60:
        Type = "Retired"
        # Retired return 90% of the times, yes means they already came
        if Rnd <= 58:
            clientid = "returning"
        else:
            clientid = "ID" + str(random.randrange(100000, 999999))
    elif Rnd >= 61:
        Type = "Onetime"
        clientid = "ID" + str(random.randrange(100000, 999999))

    # determination of starter dishes
    alea1 = random.randrange(1, 100)
    if Type == "Business":
        # 75% of business take starters, 90% of them take oysters
        if alea1 < 75 * 0.9:
            Starter = "Oysters"
        elif alea1 < 75:
            Starter = "Soup"
        else:
            Starter = "Nothing"

    # 100% of healthy group take salads
    elif Type == "Healthy":
        Starter = "Soup"

    # 0% of Onetime take starters
    elif Type == "Onetime":
        Starter = "Nothing"

    # 90% of retired take starters
    elif Type == "Retired":
        if alea1 <= 90 * 0.4:
            Starter = "Oysters"
        elif alea1 <= 90 * 0.7:
            Starter = "Tomato"
        elif alea1 <= 90:
            Starter = "Soup"
        else:
            Starter = "Nothing"

    # determination of main dishes
    # 74% take lobster 8% steak or Spaghetti or salad
    alea2 = random.randrange(1, 100)
    if Type == "Business":
        if alea2 <= 76:
            main = "Lobster"
        elif alea2 <= 84:
            main = "Spaghetti"
        elif alea2 <= 92:
            main = "Steak"
        else:
            main = "Salad"

    # 100% take salad
    if Type == "Healthy":
        main = "Salad"

    # 7% take Lobster, 65% Spaghetti, 21% steak, 7% salad
    if Type == "Retired":
        if alea2 <= 7:
            main = "Lobster"
        elif alea2 <= 72:
            main = "Spaghetti"
        elif alea2 <= 93:
            main = "Steak"
        else:
            main = "Salad"

    # 80% take Spaghetti, 10% Lobster or Spaghetti
    if Type == "Onetime":
        if alea2 <= 10:
            main = "Lobster"
        elif alea2 <= 20:
            main = "Spaghetti"
        else:
            main = "Steak"

    # determination of dessert dishes
    # 90% take Pie 80% take dessert
    alea3 = random.randrange(1, 100)
    if Type == "Business":
        if alea3 <= 80 * 0.9:
            dessert = "Pie"
        elif alea3 <= 80:
            dessert = "Ice"
        else:
            dessert = "Nothing"
    # they take no dessert
    if Type == "Onetime":
        dessert = "Nothing"

    # 2% take dessert but only ice
    if Type == "Healthy":
        if alea3 <= 2:
            dessert = "Ice"
        else:
            dessert = "Nothing"

    # all take dessert 90% take pie
    if Type == "Retired":
        if alea3 <= 90:
            dessert = "Pie"
        else:
            dessert = "Ice"
    Id_list.append(clientid)
    Type_list.append(Type)
    starter_list.append(Starter)
    main_list.append(main)
    dessert_list.append(dessert)
    i = gorestaurant(clientid, Type, Starter, main, dessert)

# dictionary, upload this data on csv
dico = {'ID': Id_list, 'Type': Type_list, 'FIRST_COURSE': starter_list, 'SECOND_COURSE': main_list, 'THIRD_COURSE': dessert_list}
df = pd.DataFrame(dico)
df.to_csv('Data/Simulation.csv')

# find the price of dishes
df_starter = pd.read_csv('Data/Simulation.csv', usecols=[3])
df_main = pd.read_csv('Data/Simulation.csv', usecols=[4])
df_dessert = pd.read_csv('Data/Simulation.csv', usecols=[5])

starter_price = df_starter.to_numpy()
main_price = df_main.to_numpy()
dessert_price = df_dessert.to_numpy()
starter_arr = []
main_arr = []
dessert_arr = []

# find prices of starters and add the column
s_revenue = 0
for x in starter_price:

    if x == "Soup":
        starter_arr.append(3)
        s_revenue += 3
    elif x == "Tomato":
        starter_arr.append(15)
        s_revenue += 15
    elif x == "Oysters":
        starter_arr.append(20)
        s_revenue += 20
    elif x == "Nothing":
        starter_arr.append(0)
starter_price = np.array(starter_arr, dtype='object')
df['starter_price'] = starter_price.tolist()

# find prices of main and add the column
m_revenue= 0
for x in main_price:

    if x == "Salad":
        main_arr.append(9)
        m_revenue += 9
    elif x == "Spaghetti":
        main_arr.append(20)
        m_revenue += 20
    elif x == "Lobster":
        main_arr.append(40)
        m_revenue += 40
    elif x == "Nothing":
        main_arr.append(0)
    elif x == "Steak":
        main_arr.append(25)
        m_revenue += 25
main_price = np.array(main_arr, dtype='object')
df['main_price'] = main_price.tolist()

# find prices of dessert and add the column
d_revenue = 0
for x in dessert_price:

    if x == "Pie":
        dessert_arr.append(10)
        d_revenue += 10
    elif x == "Ice":
        dessert_arr.append(15)
        d_revenue += 15
    elif x == "Nothing":
        dessert_arr.append(0)
dessert_price = np.array(dessert_arr, dtype='object')
df['dessert_price'] = dessert_price.tolist()

# add the drinks
starter_drink = []
main_drink = []
dessert_drink = []

for i in starter_price:
    if i == 0:
        starter_drink.append(0)
    else:
        starter_drink.append(random.uniform(0, 5))
starter_price = np.array(starter_drink, dtype='object')
df['starter_drink'] = starter_price.tolist()

for i in main_price:
    if i == 0:
        main_drink.append(0)
    else:
        main_drink.append(random.uniform(0, 5))
main_price = np.array(main_drink, dtype='object')
df['main_drink'] = main_price.tolist()

for i in dessert_price:
    if i == 0:
        dessert_drink.append(0)
    else:
        dessert_drink.append(random.uniform(0, 5))
dessert_price = np.array(dessert_drink, dtype='object')
df['dessert_drink'] = dessert_price.tolist()

df.to_csv('Data/Simulation.csv')


# analysis
import matplotlib.pyplot as plt

nb_business = 0
nb_onetime = 0
nb_healthy = 0
nb_retirement = 0
for i in Type_list:
    if i == "Business":
        nb_business = nb_business + 1
    elif i == "Onetime":
        nb_onetime = nb_onetime + 1
    elif i == "Healthy":
        nb_healthy = nb_healthy + 1
    elif i == "Retired":
        nb_retirement = nb_retirement + 1

    # display the distribution
plt.bar(["Business", "Onetime", "Healthy", "Retirement"],
        [nb_business / len(df.index), nb_onetime / len(df.index),
         nb_healthy / len(df.index), nb_retirement / len(df.index)])
plt.title('Repartition of types')
plt.ylabel('Distribution')
plt.xlabel('Types')
plt.show()

# print the revenue
print("Revenue from 1st course is " + str(s_revenue))
print("Revenue from 2nd course is " + str(m_revenue))
print("Revenue from 3rd course is " + str(d_revenue))

