import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# read from CSV file dataframes

df = pd.read_csv('Data/part1.csv', index_col=0)
df_courses = pd.read_csv('Data/part1.csv', usecols=[2, 3, 4])
df_time = pd.read_csv('Data/part1.csv', usecols=[1])

# sum total cost of each course

tot_course = df_courses.sum(axis=1)

time = df_time.to_numpy()
dinner_tot = 0
lunch_tot = 0

dinner_avg = 0
lunch_avg = 0
count = 0

# calculate individual total of the courses

for i in time:

    if i == "DINNER":
        dinner_tot += tot_course[count]
    elif i == "LUNCH":
        lunch_tot += tot_course[count]
    count += 1

# get the average of each individual cost of the courses

dinner_avg = dinner_tot / count
lunch_avg = lunch_tot / count

# display the total cost of courses

plt.bar(["dinner", "lunch"], [dinner_tot, lunch_tot])
plt.title('total cost for each course')
plt.ylabel('cost')
plt.xlabel('courses')
plt.show()

# display the average cost per course

plt.bar(["dinner", "lunch"], [dinner_avg, lunch_avg])
plt.title('average cost for each course')
plt.ylabel('cost')
plt.xlabel('courses')
plt.show()

# display the distribution of course prices
bins = [x + 0.5 for x in range(-1, 50)]
plt.hist(df["FIRST_COURSE"], bins=bins, color='yellow', edgecolor='black', hatch='-', label='First Course')
plt.hist(df["SECOND_COURSE"], bins=bins, color='orange', alpha=0.5, edgecolor='blue', label='Second Course')
plt.hist(df["THIRD_COURSE"], bins=bins, color='blue', alpha=0.25, edgecolor='red', hatch='/', label='Third Course')
plt.ylabel('Frequency')
plt.xlabel('Values')
plt.title('Distribution')
plt.legend()
plt.show()

# read from CSV file sub-courses taken of the course

starter = pd.read_csv('Data/part1.csv', usecols=[2])
main = pd.read_csv('Data/part1.csv', usecols=[3])
dessert = pd.read_csv('Data/part1.csv', usecols=[4])

# get the cost of drinks for starters

starter_drinks = starter.to_numpy()
starter_arr = []
for x in starter_drinks:
    val = 0
    if x == 0:
        starter_arr.append(float(val))
    elif x < 15:
        val = x - 3
        starter_arr.append(float(val))
    elif x < 20:
        val = x - 15
        starter_arr.append(float(val))
    else:
        val = x - 20
        starter_arr.append(float(val))

# get the cost of drinks for the main

main_drinks = main.to_numpy()
main_arr = []
for x in main_drinks:
    val = 0
    if x == 0:
        main_arr.append(float(val))
    elif x < 20:
        val = x - 9
        main_arr.append(float(val))
    elif x < 25:
        val = x - 20
        main_arr.append(float(val))
    elif x < 40:
        val = x - 25
        main_arr.append(float(val))
    else:
        val = x - 40
        main_arr.append(float(val))

# get the cost of drinks for dessert

dessert_drinks = dessert.to_numpy()
dessert_arr = []
for x in dessert_drinks:
    val = 0
    if x == 0:
        dessert_arr.append(float(val))
    elif x < 15:
        val = x - 10
        dessert_arr.append(float(val))
    else:
        val = x - 15
        dessert_arr.append(float(val))

# add the starter drinks to the data

starter_drinks = np.array(starter_arr, dtype='object')
df['starter-drinks'] = starter_drinks.tolist()

# add the main drinks to the data

main_drinks = np.array(main_arr, dtype='object')
df['main-drinks'] = main_drinks.tolist()

# add the dessert drink to the data

dessert_drinks = np.array(dessert_arr, dtype='object')
df['dessert-drinks'] = dessert_drinks.tolist()

# get the cost of the starter dish

starter_dish = starter.to_numpy()
starter_dish_arr = []
for x in starter_dish:
    val = 0
    if x == 0:
        starter_dish_arr.append(val)
    elif x < 15:
        val = 3
        starter_dish_arr.append(val)
    elif x < 20:
        val = 15
        starter_dish_arr.append(val)
    else:
        val = 20
        starter_dish_arr.append(val)

# get the cost of the main dish

main_dish = main.to_numpy()
main_dish_arr = []
for x in main_dish:
    val = 0
    if x == 0:
        main_dish_arr.append(val)
    elif x < 20:
        val = 9
        main_dish_arr.append(val)
    elif x < 25:
        val = 20
        main_dish_arr.append(val)
    elif x < 40:
        val = 25
        main_dish_arr.append(val)
    else:
        val = 40
        main_dish_arr.append(val)

# get the cost of the dessert dish

dessert_dish = dessert.to_numpy()
dessert_dish_arr = []
for x in dessert_dish:
    val = 0
    if x == 0:
        dessert_dish_arr.append(val)
    elif x < 15:
        val = 10
        dessert_dish_arr.append(val)
    else:
        val = 15
        dessert_dish_arr.append(val)

# add starter dish to the data

starter_dish = np.array(starter_dish_arr, dtype='object')
df['starter-dish'] = starter_dish.tolist()

# add main dish to the data

main_dish = np.array(main_dish_arr, dtype='object')
df['main-dish'] = main_dish.tolist()

# add dessert dish to the data

dessert_dish = np.array(dessert_dish_arr, dtype='object')
df['dessert-dish'] = dessert_dish.tolist()

# add all modifications to the CSV file

df.to_csv('Data/part1.csv')

# display modifications
print("Kindly check the columns that have been added to the excel file part 1 ")

