import pandas as pd
import matplotlib.pyplot as plt
from Part1 import df, starter, main, dessert

# read the csv
df2 = pd.read_csv('Data/part3.csv', index_col=0)

# Merge the dataframes
MergedDf = df.merge(df2, how='outer', left_index=True, right_index=True)

df_type = pd.read_csv('Data/part3.csv', usecols=[1])
# analyse the type column

type = df_type.to_numpy()

nb_business = 0
nb_onetime = 0
nb_healthy = 0
nb_retirement = 0

# count the number of clients in each category
for i in type:

    if i == "Business":
        nb_business = nb_business + 1
    elif i == "Onetime":
        nb_onetime = nb_onetime + 1
    elif i == "Healthy":
        nb_healthy = nb_healthy + 1
    elif i == "Retirement":
        nb_retirement = nb_retirement + 1

# display the distribution
plt.bar(["Business", "Onetime", "Healthy", "Retirement"],
        [nb_business / len(df_type.index), nb_onetime / len(df_type.index),
         nb_healthy / len(df_type.index), nb_retirement / len(df_type.index)])
plt.title('Repartition of types')
plt.ylabel('Distribution')
plt.xlabel('Types')
plt.show()

# print(nb_healthy)
# print(nb_retirement)
# print(nb_business)
# print(nb_onetime)


# get each course's dataframe as an array
starter_prob = starter.to_numpy()

main_prob = main.to_numpy()

dessert_prob = dessert.to_numpy()

# calculate the probability of taking all the courses for each type
prob_business_all_courses = 0
prob_onetime_all_courses = 0
prob_health_all_courses = 0
prob_retire_all_courses = 0

count = 0

for i in type:

    if i == "Business":
        if starter_prob[count] > 0 and main_prob[count] > 0 and dessert_prob[count] > 0:
            prob_business_all_courses += 1

    elif i == "Onetime":
        if starter_prob[count] > 0 and main_prob[count] > 0 and dessert_prob[count] > 0:
            prob_onetime_all_courses += 1

    elif i == "Healthy":
        if starter_prob[count] > 0 and main_prob[count] > 0 and dessert_prob[count] > 0:
            prob_health_all_courses += 1

    elif i == "Retirement":
        if starter_prob[count] > 0 and main_prob[count] > 0 and dessert_prob[count] > 0:
            prob_retire_all_courses += 1

    count += 1

prob_business_all_courses = prob_business_all_courses / nb_business
prob_onetime_all_courses = prob_onetime_all_courses / nb_onetime
prob_health_all_courses = prob_health_all_courses / nb_healthy
prob_retire_all_courses = prob_retire_all_courses / nb_retirement


# display the probability
plt.bar(["Business", "Onetime", "Healthy", "Retirement"], [prob_business_all_courses, prob_onetime_all_courses,
                                                           prob_health_all_courses, prob_retire_all_courses])
plt.title('the probability of taking all courses for each type of client')
plt.ylabel('probability of taking all courses')
plt.xlabel('Type of client')
plt.show()

# calculate the probability of taking a course for each type of client
prob_business_starter = 0
prob_onetime_starter = 0
prob_health_starter = 0
prob_retire_starter = 0

prob_business_main = 0
prob_onetime_main = 0
prob_health_main = 0
prob_retire_main = 0

prob_business_dessert = 0
prob_onetime_dessert = 0
prob_health_dessert = 0
prob_retire_dessert = 0

count = 0

for i in type:

    if i == "Business":
        if starter_prob[count] > 0:
            prob_business_starter += float(1 / nb_business)

        if main_prob[count] > 0:
            prob_business_main += float(1 / nb_business)

        if dessert_prob[count] > 0:
            prob_business_dessert += float(1 / nb_business)

    elif i == "Onetime":
        if starter_prob[count] > 0:
            prob_onetime_starter += float(1 / nb_onetime)

        if main_prob[count] > 0:
            prob_onetime_main += float(1 / nb_onetime)

        if dessert_prob[count] > 0:
            prob_onetime_dessert += float(1 / nb_onetime)

    elif i == "Healthy":
        if starter_prob[count] > 0:
            prob_health_starter += float(1 / nb_healthy)

        if main_prob[count] > 0:
            prob_health_main += float(1 / nb_healthy)

        if dessert_prob[count] > 0:
            prob_health_dessert += float(1 / nb_healthy)

    elif i == "Retirement":
        if starter_prob[count] > 0:
            prob_retire_starter += float(1 / nb_retirement)

        if main_prob[count] > 0:
            prob_retire_main += float(1 / nb_retirement)

        if dessert_prob[count] > 0:
            prob_retire_dessert += float(1 / nb_retirement)

    count += 1

# display the probability of taking each course for each type of client
plt.bar(["Business", "Onetime", "Healthy", "Retirement"], [prob_business_starter, prob_onetime_starter,
                                                           prob_health_starter, prob_retire_starter])
plt.title('probability of taking the starter course for each type of client')
plt.ylabel('probability of taking starter')
plt.xlabel('Types')
plt.show()


plt.bar(["Business", "Onetime", "Healthy", "Retirement"], [prob_business_main, prob_onetime_main,
                                                           prob_health_main, prob_retire_main])
plt.title('probability of taking main course for each type of client')
plt.ylabel('probability of taking main')
plt.xlabel('Types')
plt.show()


plt.bar(["Business", "Onetime", "Healthy", "Retirement"], [prob_business_dessert, prob_onetime_dessert,
                                                           prob_health_dessert, prob_retire_dessert])
plt.title('probability of taking dessert course for each type of client')
plt.ylabel('probability of taking dessert')
plt.xlabel('Types')
plt.show()

# Distribution of the cost of drinks per course
df_starter_drink = pd.read_csv('Data/part1.csv', usecols=[5])
df_main_drink = pd.read_csv('Data/part1.csv', usecols=[6])
df_dessert_drink = pd.read_csv('Data/part1.csv', usecols=[7])

bins = [x + 0.5 for x in range(-1, 10)]
plt.hist(df_starter_drink, bins=bins, color='yellow', edgecolor='black', hatch='-', label='Starter Drink')
plt.hist(df_main_drink, bins=bins, color='orange', alpha=0.5, edgecolor='blue', label='Main Drink')
plt.hist(df_dessert_drink, bins=bins, color='blue', alpha=0.25, edgecolor='red', hatch='/', label='Dessert Drink')
plt.ylabel('Frequency')
plt.xlabel('Values')
plt.title('Distribution')
plt.legend()
plt.show()

# calculate the probability of taking a certain dish for each type of client
# starters
business_soup = 0
onetime_soup = 0
health_soup = 0
retire_soup = 0

business_tomato = 0
onetime_tomato = 0
health_tomato = 0
retire_tomato = 0

business_oysters = 0
onetime_oysters = 0
health_oysters = 0
retire_oysters = 0

# main
business_salad = 0
onetime_salad = 0
health_salad = 0
retire_salad = 0

business_Spaghetti = 0
onetime_Spaghetti = 0
health_Spaghetti = 0
retire_Spaghetti = 0

business_Steak = 0
onetime_Steak = 0
health_Steak = 0
retire_Steak = 0

business_Lobster = 0
onetime_Lobster = 0
health_Lobster = 0
retire_Lobster = 0

business_Ice = 0
onetime_Ice = 0
health_Ice = 0
retire_Ice = 0

business_Pie = 0
onetime_Pie = 0
health_Pie = 0
retire_Pie = 0

count = 0

for i in type:

    if i == "Business":
        if starter_prob[count] > 0:
            if starter_prob[count] >= 20:
                business_oysters += 1
            elif starter_prob[count] >= 15:
                business_tomato += 1
            else:
                business_soup += 1

        if main_prob[count] > 0:
            if main_prob[count] >= 40:
                business_Lobster += 1
            elif main_prob[count] >= 25:
                business_Steak += 1
            elif main_prob[count] >= 20:
                business_Spaghetti += 1
            else:
                business_salad += 1

        if dessert_prob[count] > 0:
            if dessert_prob[count] >= 15:
                business_Ice += 1
            else:
                business_Pie += 1

    elif i == "Onetime":
        if starter_prob[count] > 0:
            if starter_prob[count] >= 20:
                onetime_oysters += 1
            elif starter_prob[count] >= 15:
                onetime_tomato += 1
            else:
                onetime_soup += 1

        if main_prob[count] > 0:
            if main_prob[count] >= 40:
                onetime_Lobster += 1
            elif main_prob[count] >= 25:
                onetime_Steak += 1
            elif main_prob[count] >= 20:
                onetime_Spaghetti += 1
            else:
                onetime_salad += 1

        if dessert_prob[count] > 0:
            if dessert_prob[count] >= 15:
                onetime_Ice += 1
            else:
                onetime_Pie += 1

    elif i == "Healthy":
        if starter_prob[count] > 0:
            if starter_prob[count] >= 20:
                health_oysters += 1
            elif starter_prob[count] >= 15:
                health_tomato += 1
            else:
                health_soup += 1

        if main_prob[count] > 0:
            if main_prob[count] >= 40:
                health_Lobster += 1
            elif main_prob[count] >= 25:
                health_Steak += 1
            elif main_prob[count] >= 20:
                health_Spaghetti += 1
            else:
                health_salad += 1

        if dessert_prob[count] > 0:
            if dessert_prob[count] >= 15:
                health_Ice += 1
            else:
                health_Pie += 1

    elif i == "Retirement":
        if starter_prob[count] > 0:
            if starter_prob[count] >= 20:
                retire_oysters += 1
            elif starter_prob[count] >= 15:
                retire_tomato += 1
            else:
                retire_soup += 1

        if main_prob[count] > 0:
            if main_prob[count] >= 40:
                retire_Lobster += 1
            elif main_prob[count] >= 25:
                retire_Steak += 1
            elif main_prob[count] >= 20:
                retire_Spaghetti += 1
            else:
                retire_salad += 1

        if dessert_prob[count] > 0:
            if dessert_prob[count] >= 15:
                retire_Ice += 1
            else:
                retire_Pie += 1
    count += 1

# compute the probabilities
business_starter_tot = business_oysters + business_soup + business_tomato
healthy_starter_tot = health_oysters + health_soup + health_tomato
retire_starter_tot = retire_oysters + retire_soup + retire_tomato
onetime_starter_tot = onetime_oysters + onetime_soup + onetime_tomato

business_main_tot = business_Lobster + business_Spaghetti + business_Steak + business_salad
healthy_main_tot = health_Lobster + health_Spaghetti + health_Steak + health_salad
retire_main_tot = retire_Lobster + retire_Spaghetti + retire_Steak + retire_salad
onetime_main_tot = onetime_Lobster + onetime_Spaghetti + onetime_Steak + onetime_salad

business_dessert_tot = business_Pie + business_Ice
healthy_dessert_tot = health_Pie + health_Ice
retire_dessert_tot = retire_Pie + retire_Ice
onetime_dessert_tot = onetime_Pie + onetime_Ice

# print the starter graph
plt.bar(["Business Oysters ", "Business Soup", "Business Tomato", "Healthy Oysters ", "Healthy Soup",
         "Healthy Tomato", "Retire Oysters ", "Retire Soup", "Retire Tomato", "Onetime Oysters ", "Onetime Soup",
         "Onetime Tomato"], [business_oysters/business_starter_tot, business_soup/business_starter_tot,
                             business_tomato/business_starter_tot, health_oysters/healthy_starter_tot,
                             health_soup/healthy_starter_tot, health_tomato/healthy_starter_tot,
                             retire_oysters/retire_starter_tot, retire_soup/retire_starter_tot,
                             retire_tomato/retire_starter_tot, onetime_oysters/onetime_starter_tot,
                             onetime_soup/onetime_starter_tot, onetime_tomato/onetime_starter_tot])
plt.title('probability of taking a specific starter')
plt.ylabel('probability of taking starter')
plt.xlabel('Starters')
plt.show()

# print the main graph
plt.bar(["b_Lobster", "b_Spaghetti", "b_Steak", "b_salad", "h_Lobster", "h_Spaghetti", "h_Steak", "h_salad",
         "r_Lobster", "r_Spaghetti", "r_Steak", "r_salad", "o_Lobster", "o_Spaghetti", "o_Steak", "o_salad"],
        [business_Lobster/business_main_tot, business_Spaghetti/business_main_tot, business_Steak/business_main_tot,
         business_salad/business_main_tot, health_Lobster/healthy_main_tot, health_Spaghetti/healthy_main_tot,
         health_Steak/healthy_main_tot, health_salad/healthy_main_tot, retire_Lobster/retire_main_tot,
         retire_Spaghetti/retire_main_tot, retire_Steak/retire_main_tot, retire_salad/retire_main_tot,
         onetime_Lobster/onetime_main_tot, onetime_Spaghetti/onetime_main_tot, onetime_Steak/onetime_main_tot,
         onetime_salad/onetime_main_tot])
plt.title('probability of taking a specific main')
plt.ylabel('probability of taking main')
plt.xlabel('Mains')
plt.show()

# plot the dessert graph
plt.bar(["business_Pie", "business_Ice", "health_Pie", "health_Ice", "retire_Pie", "retire_Ice", "onetime_Pie",
         "onetime_Ice"], [business_Pie/business_dessert_tot, business_Ice/business_dessert_tot,
        health_Pie/healthy_dessert_tot, health_Ice/healthy_dessert_tot, retire_Pie/retire_dessert_tot,
        retire_Ice/retire_dessert_tot, onetime_Pie/onetime_dessert_tot, onetime_Ice/onetime_dessert_tot])
plt.title('probability of taking a specific dessert')
plt.ylabel('probability of taking dessert')
plt.xlabel('Dessert')
plt.show()
